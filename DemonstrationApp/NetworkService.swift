//
//  NetworkService.swift
//  DemonstrationApp
//
//  Created by Руслан on 19.11.2022.
//

import Foundation

protocol INetworkService: AnyObject {
    func doSomeStuff() -> Int
}

final class NetworkService: INetworkService {

    func doSomeStuff() -> Int {
        // здесь может быть более сложная логика
        return 0
    }
}
