//
//  Presenter.swift
//  DemonstrationApp
//
//  Created by Руслан on 19.11.2022.
//

import Foundation

protocol IPresenter: AnyObject {
    var dataSource: [DataSourceItem] { get }
    var shouldShowOnboarding: Bool { get }

    func viewDidLoad()
    func didTapButton()
}

final class Presenter: IPresenter {

    // Dependencies
    weak var view: IView?
    private let networkService: INetworkService
    private let databaseService: IDatabaseService

    // Properties
    var dataSource: [DataSourceItem] = []
    var shouldShowOnboarding: Bool = true

    // MARK: Init

    init(networkService: INetworkService, databaseService: IDatabaseService) {
        self.networkService = networkService
        self.databaseService = databaseService
    }

    // MARK: IPresenter

    func viewDidLoad() {
        // здесь может быть более сложная логика
        let number = networkService.doSomeStuff()
        view?.showNumber(number)
    }

    func didTapButton() {
        // здесь может быть более сложная логика
        let string = databaseService.doSomeAnotherStuff(with: dataSource)
        view?.showString(string)
    }
}
