//
//  ViewController.swift
//  DemonstrationApp
//
//  Created by Руслан on 19.11.2022.
//

import UIKit

struct DataSourceItem: Equatable {
    let title: String
    let image: UIImage?
}

protocol IView: AnyObject {
    func showNumber(_ number: Int)
    func showString(_ string: String)
}

final class ViewController: UIViewController, IView {

    func showNumber(_ number: Int) {
        // здесь может быть более сложная логика
        print(number)
    }

    func showString(_ string: String) {
        // здесь может быть более сложная логика
        print(string)
    }
}
