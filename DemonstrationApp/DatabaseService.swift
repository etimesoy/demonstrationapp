//
//  DatabaseService.swift
//  DemonstrationApp
//
//  Created by Руслан on 19.11.2022.
//

import Foundation

protocol IDatabaseService: AnyObject {
    func doSomeAnotherStuff(with dataSource: [DataSourceItem]) -> String
}

final class DatabaseService: IDatabaseService {

    func doSomeAnotherStuff(with dataSource: [DataSourceItem]) -> String {
        // здесь может быть более сложная логика
        return "something"
    }
}
