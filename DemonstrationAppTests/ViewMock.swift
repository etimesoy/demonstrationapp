//
//  ViewMock.swift
//  DemonstrationAppTests
//
//  Created by Руслан on 19.11.2022.
//

import UIKit
@testable import DemonstrationApp

final class ViewMock: IView {

    var invokedShowNumber = false
    var invokedShowNumberCount = 0
    var invokedShowNumberParameters: (number: Int, Void)?
    var invokedShowNumberParametersList: [(number: Int, Void)] = []

    func showNumber(_ number: Int) {
        invokedShowNumber = true
        invokedShowNumberCount += 1
        invokedShowNumberParameters = (number, ())
        invokedShowNumberParametersList.append((number, ()))
    }

    var invokedShowString = false
    var invokedShowStringCount = 0
    var invokedShowStringParameters: (string: String, Void)?
    var invokedShowStringParametersList: [(string: String, Void)] = []

    func showString(_ string: String) {
        invokedShowString = true
        invokedShowStringCount += 1
        invokedShowStringParameters = (string, ())
        invokedShowStringParametersList.append((string, ()))
    }
}
