//
//  DatabaseServiceMock.swift
//  DemonstrationAppTests
//
//  Created by Руслан on 19.11.2022.
//

import Foundation
@testable import DemonstrationApp

final class DatabaseServiceMock: IDatabaseService {

    var invokedDoSomeAnotherStuff = false
    var invokedDoSomeAnotherStuffCount = 0
    var invokedDoSomeAnotherStuffParameters: (dataSource: [DataSourceItem], Void)?
    var invokedDoSomeAnotherStuffParametersList: [(dataSource: [DataSourceItem], Void)] = []
    var stubbedDoSomeAnotherStuffResult: String!

    func doSomeAnotherStuff(with dataSource: [DataSourceItem]) -> String {
        invokedDoSomeAnotherStuff = true
        invokedDoSomeAnotherStuffCount += 1
        invokedDoSomeAnotherStuffParameters = (dataSource, ())
        invokedDoSomeAnotherStuffParametersList.append((dataSource, ()))
        return stubbedDoSomeAnotherStuffResult
    }
}
