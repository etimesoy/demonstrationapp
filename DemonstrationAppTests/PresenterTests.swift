//
//  PresenterTests.swift
//  DemonstrationAppTests
//
//  Created by Руслан on 19.11.2022.
//

import UIKit
import XCTest
@testable import DemonstrationApp

final class PresenterTests: XCTestCase {

    private var networkServiceMock: NetworkServiceMock!
    private var databaseServiceMock: DatabaseServiceMock!
    private var viewMock: ViewMock!
    // system under test
    private var sut: IPresenter!

    override func setUp() {
        super.setUp()

        networkServiceMock = NetworkServiceMock()
        databaseServiceMock = DatabaseServiceMock()
        viewMock = ViewMock()
        let presenter = Presenter(networkService: networkServiceMock, databaseService: databaseServiceMock)
        presenter.view = viewMock
        sut = presenter
    }

    override func tearDown() {
        super.tearDown()

        networkServiceMock = nil
        databaseServiceMock = nil
        viewMock = nil
        sut = nil
    }

    // MARK: Tests

    func test_dataSource() {
        // given
        let expectedDataSource: [DataSourceItem] = []

        // when
        let dataSource = sut.dataSource

        // then
        XCTAssertEqual(dataSource, expectedDataSource)
    }

    func test_shouldShowOnboarding() {
        // given
        let expectedShouldShowOnboarding = true

        // when
        let shouldShowOnboarding = sut.shouldShowOnboarding

        // then
        XCTAssertEqual(shouldShowOnboarding, expectedShouldShowOnboarding)
    }

    func test_viewDidLoad() {
        // given
        let expectedNumber = 100
        networkServiceMock.stubbedDoSomeStuffResult = expectedNumber

        // when
        sut.viewDidLoad()

        // then
        XCTAssertTrue(viewMock.invokedShowNumber)
        XCTAssertEqual(viewMock.invokedShowNumberCount, 1)
        XCTAssertEqual(viewMock.invokedShowNumberParameters?.number, expectedNumber)
    }

    func test_didTapButton() {
        // given
        let expectedString = "сорри что поздно сдаю документ 🥺🥺🥺"
        databaseServiceMock.stubbedDoSomeAnotherStuffResult = expectedString

        // when
        sut.didTapButton()

        // then
        XCTAssertTrue(viewMock.invokedShowString)
        XCTAssertEqual(viewMock.invokedShowStringCount, 1)
        XCTAssertEqual(viewMock.invokedShowStringParameters?.string, expectedString)
    }
}
