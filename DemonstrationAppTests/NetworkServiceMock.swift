//
//  NetworkServiceMock.swift
//  DemonstrationAppTests
//
//  Created by Руслан on 19.11.2022.
//

import Foundation
@testable import DemonstrationApp

final class NetworkServiceMock: INetworkService {

    var invokedDoSomeStuff = false
    var invokedDoSomeStuffCount = 0
    var stubbedDoSomeStuffResult: Int!

    func doSomeStuff() -> Int {
        invokedDoSomeStuff = true
        invokedDoSomeStuffCount += 1
        return stubbedDoSomeStuffResult
    }
}
